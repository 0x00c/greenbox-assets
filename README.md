# Greenbox Assets

### Über dieses Projekt

In diesem Repository findest du alle offiziellen Grafiken rund um die Greenbox. Außerdem Grafiken, die im laufe der Zeit entstanden sind. Dazu zählen die Boxen, Banner, Profilbilder und sonstige Grafiken. 

### Was kann ich mit diesen Assets tun?

Du kannst die Grafiken in diesem Projekt frei in deinen Projekten verwenden. Egal ob du sie für Fanart, Wallpaper oder Community-Projekte nutzen möchtest.



Viel Spaß!